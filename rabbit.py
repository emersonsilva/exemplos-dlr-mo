import os, json, requests
import pika
RABBITMQ_USER = os.getenv("RABBITMQ_USER", "guest")
RABBITMQ_PASS = os.getenv("RABBITMQ_PASS", "guest")
RABBITMQ_HOST = os.getenv("RABBITMQ_HOST", "localhost:15672")
print("export RABBITMQ_USER="+RABBITMQ_USER)
print("export RABBITMQ_PASS="+RABBITMQ_PASS)
print("export RABBITMQ_HOST="+RABBITMQ_HOST)
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host=RABBITMQ_HOST))
channel = connection.channel()
 
def rebre():
    global channel
    if channel.is_closed:
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=RABBITMQ_HOST))
        channel = connection.channel()
def publish(exchange, routing, objeto):
    rebre()
    channel.basic_publish(exchange=exchange,
                    routing_key=routing,
                    body=json.dumps(objeto))

    return None
    response = requests.post("http://{USER}:{PASS}@{HOST}/api/exchanges/%2F/{EXCHANGE}/publish".format(
            USER=RABBITMQ_USER,
            PASS=RABBITMQ_PASS,
            HOST=RABBITMQ_HOST,
            EXCHANGE=exchange
    ),data=json.dumps({"properties":{},"routing_key": routing,"payload": json.dumps(objeto).replace("Hello!","__"),"payload_encoding":"string"}),
                            headers={"content-type": "application/json"})
    print(response.status_code, response.text)
    if response.status_code == 200:
        print ("DLR INSERIDA COM SUCESSO NO RABBIT")
    else:
        raise Exception("Erro on login")
