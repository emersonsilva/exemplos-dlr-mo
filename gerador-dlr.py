import rabbit
import json
import redis
import os
import time
REDIS_URL = os.getenv("REDIS_URL")
_redis = redis.Redis(REDIS_URL,decode_responses=True)

mensagens_para_dlr = _redis.keys("dlr:*")
#print("objetos das Mensagens para dlr", mensagens_para_dlr)
print("ids das mensagens para dlr", mensagens_para_dlr)
id = input("Digite o id da mensagem: ")
status = input("Digite o status: DELIVRD | UNDELIV | ACCEPTD | ENROUTE : ")


exchange = 'dlr_new_messages'
_objeto_mensagem = _redis.hget(id,'message')
pacote = json.loads(json.loads(_objeto_mensagem))
routing = pacote.get("login")
_obj_para_rabbit = {"id": id, "status": status, "mensagem": pacote.get("mensagem")}
print("Objeto final para rabbit -> voce pode fazer seu proprio codigo desde que publique o objeto nesse formato", _obj_para_rabbit)
print("deve publicar na exchange" ,exchange)
print("com a routing key o login do usuario, ex:", routing)
rabbit.publish(exchange, routing, _obj_para_rabbit)
_redis.delete(id)
